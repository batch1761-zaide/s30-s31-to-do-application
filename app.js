//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");

//[SECTION] Server Setup
	//establish a connection
	const app = express();
	//define a path/address
	const port = 4000; //Assign the port 4000 to avoid conflict in the future with frontend applications that run on port 3000

//[SECTION] Entry Point Response
	//bind the connection to the designated port
	app.listen(port, () => console.log(`Server running at port: ${port}`));


//[SECTION] Database Connection
	//Connect to MongoDB Atlas
	//change the password bracket to your own database password
	mongoose.connect('mongodb+srv://nzaide:admin123@cluster0.zvugb.mongodb.net/toDo176?retryWrites=true&w=majority',{

		//options to add to avoid deprecation warnings because of mongoose/mongodb
		useNewUrlParser:true,
		useUnifiedTopology:true

	});

	//Create notifications if the connection to the db is a success or a failue:
	let db = mongoose.connection;
	//let's add an on() method from our mongoose connection to show if the connection has succeeded or failed
	db.on('error',console.error.bind(console.log( "Connection Error")));
	//once the connection is open and successful, we will output a message in the terminal:
	db.once('open',()=>console.log("Connected to MongoDB"));

	//Middleware - A middleware, in expressjs context, are methods, functions that acts and adds features to our application.
	app.use(express.json());

	//Schema
	//Before we can create documents from our apil to save into our database, we first have to determine the stucture of the documents to be written in our database. This is to ensure the consistency of our documents and avoid future errors.

	//Schema acts as a blueprint for our data/document.

	//Schema() constructor from mongoose to create a new schema object
	const taskSchema = new mongoose.Schema({

		/*
			Define the fields for our documents.
			We will also be able to determine the appropriate data type of the values.
		*/
		name: String,
		status: String

	})

	//Mongoose Model
	/*
		Models are used to connect to your api to the corresponding collection in your database. It is a representation of your collection.

		Models uses schemas to create object that correspond to the schema. By default, when creating the collection from your model, the collection name is pluralized.

		mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)

	*/

	const Task = mongoose.model("tasks", taskSchema);

	//Post route to create a new task
	app.post('/tasks',(req,res)=>{

		//When creating a new post/put or any route that requires data from the client, first console log your req.body or any part of the requests that contains the data.
		// console.log(req.body);

		//Creating a new task document by using the constructor of our Task model. This constructor should follow the schema of the model.
		let newTask = new Task({

			name: req.body.name,
			status: req.body.status

		})

			//.save() method from an object created by a model.
			//save() method will allow us to save our document by connecting to our collection via our model.

		//save() has 2 approaches: we can add an anonymous fucntion to handle the created document or errors and created documents in separate functions.
		// newTask.save((savedTask, error)=>{

		// 	if(error) {
		// 		res.send(error);
		// 	} else{
		// 		res.send(savedTask);
		// 	}

		// })

		//.then() and .catch() chain
		//.then() is used to handle the proper result/returned value of a function. If the function properly returns a value, we can have a seprate function to handle it. 
		//.catch() is used to handle/catch the error from the use of a function. So that if an error occurs, we can handle the error separately.

		newTask.save()
		.then(result => res.send({message: "Document Creation Successful."}))
		.catch(error => res.send({message: "Error in Document Creation."}));

	});

	//GET method Request to retrieve ALL task documents from our collection

	app.get('/tasks',(req,res)=>{

		//TO query using mongoose, first access the model of the cpllection you want to manipulate
		//mongodb-db.tasks.fund({})
		Task.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));

	});


//[SECTION -- FULL CLEAR CODE-BLOCK WITHOUT COMMENTS]
	const sampleSchema = new mongoose.Schema({
		name: String,
		isActive: Boolean
	})

	const Sample = mongoose.model("samplee", sampleSchema);

	app.post('/samples',(req, res)=> {

		let newSample = new Sample({

			name: req.body.name,
			isActive: req.body.isActive

		})

		newSample.save()
		.then(result => res.send({message: "Document Creation Successful."}))
		.catch(error => res.send({message: "Error in Document Creation."}
		));
	});





	app.get('/samples',(req,res)=>{

		//TO query using mongoose, first access the model of the cpllection you want to manipulate
		//mongodb-db.tasks.fund({})
		Sample.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));

	});


//[S30 ACTIVITY] -------------------------------------------------------------------- [S30 ACTIVITY]
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("user", userSchema);

app.post('/users',(req, res)=>{
	let newUser = new User({
		username: req.body.username,
		password: req.body.password
	})
	newUser.save()
	.then(result => res.send({message: "New User successfully created!"}))
	.catch(error => res.send({messahe: "Error in creating user!"}));

});

app.get('/users',(req,res)=>{
	User.find({})
	.then(result => res.send(result))
	.catch(error=> res.send(err));
})
