//Contains all the endpoints for our application
//We separate the routes such that "app.js" only contains information on the server

const express = require("express")

//Create a Router instance that functions as a middleware and routing system
//Allow access to HTTP method middlewares that makes it easier to create rote for our application

const router = express.Router();


const taskController = require("../controllers/taskControllers")

//-------------- [ROUTES]
//Route for getting all the tasks

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});

//Route for creating a task
	router.post("/", (req, res) => {
		taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
	});

//Route for Deleting a task
	//call out the routing component to register a brandnew endpoint.
	//When integrating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYNAMIC.
	//2 Types of End points
		//State Route
			//->unchanging, fixed, constant, steady
		//Dynamic Route
			//-> interchangeable or NOT FIXED,
router.delete('/:task',(req,res) => {
		//identify the task to be executed within this endpoint
		//call out the proper function for this route and identify the source/provider of the function.
		//DETERMINE whether the value inside the path variable is transmitted to the server.
		console.log(req.params.task);
		//place the value of the path variable inside its own container.
		let taskId = req.params.task;
		//res.send('Hello from delete') //this is only to check if the setup is correct

		//retrieve the identity of the task by 
		//taskController.deleteTask();
		//make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the information as a path variable.
		//Path variable -> this will allow us to insert data within the scope of the URL.
			//what is a variable? -> container, storage of information
		//When is a path variable useful?
			//-> when inserting only a single piece of information to REST API method that does not include a BODY section like 'GET'.

			//-> this is also useful 

			//upon checking
		taskController.deleteTask(taskId).then(resultNgDelete => res.send(resultNgDelete));
	});

//Route for Updating task status (Pending -> Complete)
	//Lets create a dynamic endpoint for this brand new route.
router.put('/:task', (req,res) => {
 	//check if you are able to acquire the values inside the path variables.
 	console.log(req.params.task); //check if the value inside the parameters of the route is properly transmitted to the server.
 	let iDNiTask = req.params.task //this is declared to smiplify the means of calling out the value of the path variable key.

 	//identify the business logic behind this task inside the controller module.
 	//call the intended controller to execute the process. make sure to identify the provider/source of the function.
 	//after invoking the controller method handle the outcome.
 	taskController.taskCompleted(iDNiTask).then(outcome => res.send(outcome));
});

 //Route for Updating task status (Completed -> Pending)
 	//This will be a counter procedure for the previous task.
 router.put('/:task/pending',(req, res)=> {
 	let id = req.params.task;
 	//console.log(id) //test the initial 
 	//decalre a business logic aspect of this brand new task in our app.
 	//involve the task you want to execute in this route.
 	taskController.taskPending(id).then(outcome => {
 		res.send(outcome);
 	});
 });

//Use "module.exports"
module.exports = router;