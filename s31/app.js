
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute")

//[SECTION] Server Setup
	const app = express();
	const port = 4000; 

	app.use(express.json());

//[SECTION] Database Connection
	mongoose.connect('mongodb+srv://nzaide:admin123@cluster0.zvugb.mongodb.net/toDo176?retryWrites=true&w=majority',{

		useNewUrlParser:true,
		useUnifiedTopology:true

	});


	//Add the task route
	//localhost:4000/taskl
	app.use("/tasks", taskRoute);

	let db = mongoose.connection;

	db.on('error',console.error.bind(console.log( "Connection Error")));
	db.once('open',()=>console.log("Connected to MongoDB"));

	app.listen(port, () => console.log(`Server running at port: ${port}`));