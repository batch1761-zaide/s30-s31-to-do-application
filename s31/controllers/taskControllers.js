//Controllers contains the functions and business logic of our Express JS Application

const Task = require("../models/task")

//[CONTROLLERS]

//[SECTION] - CREATE
		module.exports.createTask = (requestBody) => {

			let newTask = new Task({
				name: requestBody.name
			})

			return newTask.save().then((task, error) => {
				if (error) {
					return false;
				} else {
					return task;
				}
			})

		}

//[SECTION] - RETRIEVE
		module.exports.getAllTasks = () => {

			return Task.find({}).then(result => {
				return result
			})
		}


//[SECTION] - UPDATE
	//1. Change status of Task. pending -> 'completed'.
	//we will reference the document using it's ID field
	module.exports.taskCompleted = (taskId) => {
		//Query/search for the desired task to update.
		//The "findById" mongoose method will look for a resource (task) which matches the ID from the URL of the request.
		//upon performing this method inside our collection, a new promise will be instantiated, so we need to be able to handle the possible outcome of that promise.
		return Task.findById(taskId).then((found, error) => {
			//describe how were going to handle the outcome of the promise usinga selection control structure.
			//error -> rejected state of the promise
			//we will chain a thenable expression upon performing a save() method into our returned document.
			if(found) {
				console.log(found)
				found.status = 'Completed';
				return found.save().then((updatedTask, saveErr) => {
					//catch the state of the promise to identify a specific response.
					if(updatedTask){
						return 'Task status has been successfully updated'
					} else {
						return 'Task failed to update'
					}
				})
			} else {
				return 'Error! No Match Found in Document'
			}
		});
	};
	//2. Change the sattus of task (Completed -> Pending)
	module.exports.taskPending = (userInput) => {
		//expose this new component
		//search the database for the user Input.
		//findById mongoose method -> will run a search query inside our databaseusing the id field as it's reference.
		//since performing this method will have 2 possible states/outcome, we will chain a then expression to handle the possible states of the promise.
		//assign this new controller task to a its own separate route.
		console.log(`Ito ang Userinput value ${userInput}`)
		return Task.findById(userInput).then((result, err) => 
			{
				//handle and catch the state of the promise.
			if (result) {
				//process the result of the query and extract the property to modify it's value.
				result.status = 'Pending';

				//save the new changes in the document
				return result.save().then((taskUpdated, error) => {
					//create a control strcuture to identify the proper response if the updates was successfully executed.
					if (taskUpdated) {
						return `Task ${taskUpdated.name} was updated to Pending`;
					}	else {
						return 'Error when saving task updates';
					}
				})

				return `Match Found`;
			} else {
				return 'Something Went Wrong';
			}
		})
	}


//[SECTION] - DESTROY
	//1. Remove an existing resource inside the Task collection.
		//expose the data aross other modules in other app so that it will become reusable.
		//
	module.exports.deleteTask = (idNgTask) => {
		//how will the data will be processed in order to execute the task.
		//select which mongoose method will be used in order to acquire the desired end goal.
		//Mongoose -> it provides an interface and methods to be able to manipulate the resources found inside the mongoDB atlas.
		//in order to identify the location in which the function will be executed append, append the model name.

		//findByIDAndRemove => this is a mongoose method which targets a document using it's id field and removes the targeted document from the collection.
		//upon executing this method within our collection a new promise will be instatntiated upon waiting for the task to be executed, however we need to be able to handle the outcome of the promise whatever state in may fall on.
		//Promises in JS
			//=> Pending
			//=> Fulfilled
			//=> Rejected
		//to be able to handle the possible states upon executing the task.
		//we are going to insert a 'thenable' expression to determine How we will respond depending on the result of the promise.
		return Task.findByIdAndRemove(idNgTask)
		.then((fulfilled,rejected) => {
			//identify how you will act according to the outcome.
			if(fulfilled) {
				return 'Task has been successfully deleted';
			} else {
				return 'Failed to remove Task';
			};
			//assign a new endpoint for this route
		});
	};
	




